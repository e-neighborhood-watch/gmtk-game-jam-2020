module PerformMove exposing
  ( performMove
  )

import Movement exposing (..)
import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)
import Crafting exposing
  ( autoCraft
  )

cycleLeft : List a -> List a
cycleLeft xs =
  case
    xs
  of
   [] ->
     []
   head :: tail ->
     tail ++ [ head ]

cycleRight : List a -> List a
cycleRight = List.reverse >> cycleLeft >> List.reverse

-- Performs all actions that happen every turn.
--
-- That is
-- - Tries to craft 1 item
progressTurn : Level -> Level
progressTurn lvl =
  { lvl
  | playerItems =
    autoCraft lvl.recipes lvl.playerItems |> Tuple.second
  }

-- Performs a movement and checks if the level has been won
performMove : Movement -> Level -> (Bool, Level)
performMove move level =
  let
    newLevel = justMove move level
  in
    ( newLevel.playerLoc == newLevel.winLoc  -- Whether the player is standing in the win location
    , newLevel                               -- The updated level
    )

isPaper : Item -> Bool
isPaper i =
  case
    i
  of
    Schematic -> True
    Airplane -> True
    _ -> False

soakPaper : Level -> Level
soakPaper level =
  { level
  | playerItems =
    if
      obstacleAt level.playerLoc level == Just Water
    then
      List.filter (isPaper >> not) level.playerItems
    else
      level.playerItems
  }

-- Movement without win check
justMove : Movement -> Level -> Level
justMove move level =
  case
    move
  of
    -- Attempt a move
    -- MAY PROGRESS TURN
    Move direction ->
      let
        newLoc = addDirection direction level.playerLoc
        newFacing =
          case
            direction
          of
            North -> level.facingLeft
            South -> level.facingLeft
            East -> False
            West -> True
      in
        if
          validMove newLoc level
        -- Movement is possible
        -- PROGRESSES TURN
        then
          { level
          | playerLoc =
            newLoc
          , facingLeft =
            newFacing
          }
            |> progressTurn
              |> pickup
                |> soakPaper
       -- Movement fails
       -- DOES NOT PROGRESS TURN
        else
          { level
          | facingLeft =
            newFacing
          }
    -- Cycle inventory left
    -- DOES NOT PROGRESS TURN
    CycleLeft ->
      { level
      | playerItems =
        cycleLeft level.playerItems
      }
    -- Cycle inventory right
    -- DOES NOT PROGRESS TURN
    CycleRight ->
      { level
      | playerItems =
        cycleRight level.playerItems
      }
    Drop ->
      case
        itemAt level.playerLoc level
      of
        -- Item is already at the player's location
        -- DOES NOT PROGRESS TURN
        Just i -> level
        -- Player's location is empty
        -- MAY PROGRESS TURN
        Nothing ->
          case
            (progressTurn level).playerItems
          of
            -- No item to drop
            [] ->
              level
            -- Item to drop
            -- PROGRESSES TURN
            holding :: rest ->
              { level
              | playerItems =
                rest
              , floorItems = 
                (level.playerLoc, holding) :: level.floorItems
              }
    -- Never happens since it is caught before
    -- Do nothing
    Reset ->
      level
    -- Does nothing but progress turn
    -- PROGRESSES TURN
    StandStill ->
      level |> progressTurn |> pickup |> soakPaper
    -- Does nothing
    -- DOES NOT PROGRESS TURN
    NoMove ->
      level
