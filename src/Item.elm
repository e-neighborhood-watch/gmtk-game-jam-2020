module Item exposing
  ( Item (..)
  , inventoryHtml
  , inventoryText
  )

import Html exposing
  ( Html
  , text
  , br
  )

type Item
  = Nail
  | Bench
  | Wood
  | Wire
  | BarbedWire
  | Switch
  | Fire
  | InnerTube
  | Yeast
  | Flour
  | BreadLoaf
  | BreadSlice
  | ToastSlice
  | Sandwich
  | RubberScrap
  | Valve
  | Schematic
  | Airplane
  | GlassShards
  | MoltenGlass
  | Bottle
  | MessageInABottle
  | Hammer

itemName : Item -> String
itemName item =
  case
    item
  of
    Nail ->
      "Nail"
    Bench ->
      "Bench"
    Wood ->
      "Wood"
    Wire ->
      "Wire"
    BarbedWire ->
      "Barbed wire"
    Switch ->
      "Switch"
    Fire ->
      "Fire"
    InnerTube ->
      "Innertube"
    Yeast ->
      "Yeast"
    Flour ->
      "Flour"
    BreadLoaf ->
      "Bread loaf"
    BreadSlice ->
      "Bread slice"
    ToastSlice ->
      "Toast slice"
    Sandwich ->
      "Sandwich"
    RubberScrap ->
      "Rubber scrap"
    Valve ->
      "Valve"
    Schematic ->
      "Schematic"
    Airplane ->
      "Paper airplane"
    GlassShards ->
      "Glass shards"
    MoltenGlass ->
      "Molten glass"
    Bottle ->
      "Bottle"
    MessageInABottle ->
      "Message in a bottle"
    Hammer ->
      "Hammer"

inventoryHtml : List Item -> List (Html a)
inventoryHtml items =
  case
    items
  of
    [] ->
      [br [] []]
    [ item ] ->
      [ text (itemName item) ]
    i1 :: (i2 :: is) ->
      text (itemName i1 ++ ", ") :: inventoryHtml (i2 :: is)

inventoryText : List Item -> String
inventoryText items =
  case
    items
  of
    [] ->
      ""
    [ item ] ->
      itemName item
    i1 :: (i2 :: is) ->
      itemName i1 ++ ", " ++ inventoryText (i2 :: is)
