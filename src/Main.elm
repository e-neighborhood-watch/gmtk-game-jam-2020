port module Main exposing (main)

import Audio exposing (Audio, AudioCmd)
import Browser.Events
    exposing
        ( onKeyDown
        )
import Entity
import Html exposing (Html, text, br)
import Html.Attributes as Html
import Item exposing (..)
import Json.Decode as Decode
import Json.Encode as Encode
import Level exposing (Level)
import List.Nonempty as NonEmpty exposing (Nonempty(..))
import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import Math.Vector4 as Vec4 exposing (Vec4, vec4)
import Movement exposing (..)
import PerformMove exposing (..)
import Platform.Sub as Sub
import Recipe exposing (..)
import Recipes exposing (..)
import Shader
import Task exposing (Task)
import Util exposing (..)
import WebGL
import WebGL.Texture as Texture exposing (Texture)
import Movement exposing (..)
import PerformMove exposing (..)
import Model exposing (..)
import Levels.Sequence exposing
  ( levels
  )

port audioPortToJS : Encode.Value -> Cmd msg

port audioPortFromJS : (Decode.Value -> msg) -> Sub msg

type alias PrgmFlags =
    ()

type PrgmMesgs
    = TextureLoad TextureRecord
    | TextureFail String
    | UserMovement Movement

main : Program PrgmFlags (Audio.Model PrgmMesgs PrgmModel) (Audio.Msg PrgmMesgs)
main =
    Audio.elementWithAudio
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , audio = audio
        , audioPort = { fromJS = audioPortFromJS, toJS = audioPortToJS }
        }

loadTextures : List String -> Task (String, Texture.Error) (List Texture)
loadTextures textureUrls =
  List.map Texture.load textureUrls
    |> List.map2 (\url -> Task.mapError (Tuple.pair url)) textureUrls
    |> Task.sequence

init : PrgmFlags -> ( PrgmModel, Cmd PrgmMesgs, AudioCmd PrgmMesgs )
init flags =
  let
    loadImages : Cmd PrgmMesgs
    loadImages =
      loadTextures
        [ "images/protagLeft.png"
        , "images/protagRight.png"
        , "images/protagFloatyLeft.png"
        , "images/protagFloatyRight.png"
        , "images/exit.png"
        , "images/text-items/item.png"
        , "images/item-nail.png"
        , "images/item-wood.png"
        , "images/obstacle-fire.png"
        , "images/item-bench.png"
        , "images/item-switch.png"
        , "images/item-wire.png"
        , "images/item-barbed-wire.png"
        , "images/item-flour.png"
        , "images/item-yeast.png"
        , "images/item-scrap.png"
        , "images/item-valve.png"
        , "images/item-innertube.png"
        , "images/item-paper.png"
        , "images/item-plane.png"
        , "images/item-shards.png"
        , "images/item-glass.png"
        , "images/item-bottle.png"
        , "images/item-hammer.png"
        , "images/item-message-bottle.png"
        , "images/obstacle-water.png"
        , "images/obstacle-gate.png"
        , "images/obstacle-gate-open.png"
        , "images/obstacle-fire.png"
        , "images/item-bread.png"
        , "images/item-breadslice.png"
        , "images/item-toast.png"
        , "images/item-sandwich.png"
        ]
          |> Task.attempt
            (\result ->
              case result of
                Err (url, error) ->
                  TextureFail <|
                    "Error loading texture " ++ url ++ ": " ++
                      case error of
                        Texture.LoadError ->
                          "network problem, bad file format, or missing file"
                        Texture.SizeError i j->
                          "bad (non power of two) size: (" ++ String.fromInt i ++ ", " ++ String.fromInt j ++ ")"
                Ok [protagL, protagR, protagFL, protagFR, exit, item, nail, wood, fire, bench, switch, wire, barbedWire, flour, yeast, rubberScrap, valve, innertube, schematic, airplane, glassShards, moltenGlass, bottle, hammer, message, water, gate, gateOpen, fireObstacle, breadLoaf, breadSlice, toastSlice, sandwich] ->
                  TextureLoad
                    { protagL = protagL
                    , protagR = protagR
                    , protagFL = protagFL
                    , protagFR = protagFR
                    , exit = exit
                    , item = item
                    , nail = nail
                    , wood = wood
                    , fire = fire
                    , wire = wire
                    , bench = bench
                    , flour = flour
                    , yeast = yeast
                    , valve = valve
                    , water = water
                    , switch = switch
                    , innertube = innertube
                    , barbedWire = barbedWire
                    , rubberScrap = rubberScrap
                    , schematic = schematic
                    , airplane = airplane
                    , glassShards = glassShards
                    , moltenGlass = moltenGlass
                    , bottle = bottle
                    , hammer = hammer
                    , message = message
                    , gate = gate
                    , gateOpen = gateOpen
                    , fireObstacle = fireObstacle
                    , breadLoaf = breadLoaf
                    , breadSlice = breadSlice
                    , toastSlice = toastSlice
                    , sandwich = sandwich
                    }
                Ok loaded ->
                  TextureFail <|
                    "Loaded incorrect number of textures: " ++ String.fromInt (List.length loaded)
            )
  in
    ( case
        levels
      of
        Nonempty headLevel _ ->
          { level = headLevel
          , textureStatus = TextureLoading
          , upcoming = levels
          }
    , loadImages
    , Audio.cmdNone
    )

update : PrgmMesgs -> PrgmModel -> ( PrgmModel, Cmd PrgmMesgs, AudioCmd PrgmMesgs )
update mesgs model =
  case mesgs of
    TextureFail error ->
      ( { model | textureStatus = TextureErrored error }
      , Cmd.none
      , Audio.cmdNone
      )
    TextureLoad texture ->
      ( { model | textureStatus = TextureLoaded texture }
      , Cmd.none
      , Audio.cmdNone
      )
    UserMovement Reset ->
      ( case
          model.upcoming
        of
          Nonempty saveState _ ->
            { model
            | level = saveState
            }
      , Cmd.none
      , Audio.cmdNone
      )
    UserMovement move ->
      let
        (win, newLevel) = performMove move model.level
      in
        if
          win
        then
          ( nextLevel model
          , Cmd.none
          , Audio.cmdNone
          )
        else
          ( { model
            | level = -- User movement should only update model
              newLevel
            }
          , Cmd.none
          , Audio.cmdNone
          )


view : PrgmModel -> Html PrgmMesgs
view model =
  let
    gridSize : Vec2
    gridSize = intPairToVec2 model.level.gridSize
  in
    case model.textureStatus of
        TextureLoaded texture ->
            let
              protagTexture : Texture
              protagTexture =
                if
                  model.level.facingLeft
                then
                  if
                    List.member InnerTube model.level.playerItems
                  then
                    texture.protagFL
                  else
                    texture.protagL
                else
                  if
                    List.member InnerTube model.level.playerItems
                  then
                    texture.protagFR
                  else
                    texture.protagR
              contentWidth : Int
              contentWidth = Tuple.first model.level.gridSize * 64
              contentHeight : Int
              contentHeight = Tuple.second model.level.gridSize * 64
              bannerToHtml : String -> List (Html PrgmMesgs)
              bannerToHtml str = String.split "\n" str |> List.map text |> List.intersperse (Html.br [] [])
            in
              Html.div 
                [ String.fromInt contentWidth ++ "px" |> Html.style "width"
                , Html.class "main"
                ]
                [ Html.div
                  [ Html.style "padding" "10px 0px 0px 0px"
                  ]
                  ( bannerToHtml model.level.banner )
                , WebGL.toHtmlWith
                  [ WebGL.alpha False
                  , WebGL.antialias
                  ]
                  [ contentWidth |> Html.width
                  , contentHeight |> Html.height
                  , Html.style "display" "block"
                  ]
                  (List.concatMap
                    (Entity.fromWalls model.level.gridSize)
                    model.level.walls
                    ++
                      List.map
                        (Entity.textureItem gridSize texture)
                        model.level.floorItems
                        ++
                          List.map
                            (Entity.textureObstacle gridSize texture model.level)
                            model.level.obstacles
                            ++
                              -- Draw the player
                              [ Entity.textureRectangle
                                (intPairToVec2 model.level.playerLoc)
                                (vec2 1 1)
                                gridSize
                                protagTexture
                              -- Draw the exit
                              , Entity.textureRectangle
                                (intPairToVec2 model.level.winLoc)
                                (vec2 1 1)
                                gridSize
                                texture.exit
                              ]
                  )
                , Html.div
                  []
                  ( [ Html.u [] [text "Inventory: "]
                    , br [] []
                    ]
                    ++ inventoryHtml model.level.playerItems
                  )
                , Html.div
                  []
                  ( [ Html.u [] [text "Recipes: "]
                    , br [] []
                    ]
                    ++ recipesHtml model.level
                  )
                ]

        TextureErrored error -> text error

        TextureLoading ->
            text "loading textures..."


subscriptions : PrgmModel -> Sub PrgmMesgs
subscriptions model =
    Sub.batch
        [ Decode.field "key" Decode.string
            |> Decode.map (toMovement >> UserMovement)
            |> onKeyDown
        ]


audio : PrgmModel -> Audio
audio model =
    Audio.silence
