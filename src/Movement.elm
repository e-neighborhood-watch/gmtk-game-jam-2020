module Movement exposing
  ( Movement (..)
  , toMovement
  , Direction (..)
  , addDirection
  )

type Direction
  = North
  | South
  | East
  | West

addDirection : Direction -> (Int, Int) -> (Int, Int)
addDirection d (x, y) =
  case
    d
  of
    North ->
      (x, y + 1)
    South ->
      (x, y - 1)
    East ->
      (x + 1, y)
    West ->
      (x - 1, y)

type Movement
  = Move Direction
  | CycleLeft
  | CycleRight
  | NoMove
  | Reset
  | Drop
  | StandStill

toMovement : String -> Movement
toMovement x =
  case 
    x
  of
    "w" -> Move North
    "ArrowUp" -> Move North
    "s" -> Move South
    "ArrowDown" -> Move South
    "d" -> Move East
    "ArrowRight" -> Move East
    "a" -> Move West
    "ArrowLeft" -> Move West
    "q" -> CycleRight
    "e" -> CycleLeft
    "r" -> Reset
    "z" -> Drop
    " " -> StandStill
    "x" -> StandStill
    _ -> NoMove
