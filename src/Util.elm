module Util exposing (..)

import Math.Vector2 as Vec2 exposing (Vec2, vec2)


intPairToVec2 : ( Int, Int ) -> Vec2
intPairToVec2 ( i, j ) =
    vec2
        (toFloat i)
        (toFloat j)
