module Recipe exposing
  ( Recipe
  )

import Item exposing
  ( Item
  , inventoryText
  )

type alias Recipe =
    { inputs : List Item
    , outputs : List Item
    }

