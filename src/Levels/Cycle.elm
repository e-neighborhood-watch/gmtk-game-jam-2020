module Levels.Cycle exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Recipe exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 2)
      , paces =
        Nonempty
        ( Horizontal 5 )
        [ Vertical -2
        , Horizontal 1
        , Vertical 4
        , Horizontal -6
        , Vertical -2
        ]
      }
    ]
  , floorItems =
    [ ( (3,4), Hammer )
    , ( (5,3), Wood )
    ]
  , obstacles =
    [ ( (2,4), Water )
    , ( (2,3), Water )
    , ( (4,4), Water )
    , ( (4,3), Water )
    , ( (6,2), Gate )
    ]
  , recipes =
    [ Recipe
      [ Schematic
      , Bottle
      ]
      [ MessageInABottle
      ]
    , Recipe
      [ MessageInABottle
      , Hammer
      ]
      [ Schematic
      ]
    , Recipe
      [ Schematic
      , Wood
      ]
      [ Switch
      ]
    , Recipe
      [ Schematic
      ]
      [ Airplane
      ]
    , Recipe
      [ GlassShards
      ]
      [ MoltenGlass
      ]
    , Recipe
      [ MoltenGlass
      ]
      [ Bottle
      ]
    , Recipe
      [ Bottle
      ]
      [ GlassShards
      ]
    , Recipe
      [ Airplane
      ]
      [ Schematic
      ]
    ]
  , playerItems =
    [ GlassShards
    , Schematic
    , InnerTube
    ]
  , playerLoc = (1, 3)
  , winLoc = (6, 1)
  , gridSize = (8,6)
  , facingLeft = True
  , banner = "The C.R.A.P. can also use custom schematics!  Unfortunately they don't fare to well in water ..."
  }
