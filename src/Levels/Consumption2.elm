module Levels.Consumption2 exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 0)
      , paces =
        Nonempty
        ( Level.Horizontal 2 )
        [ Level.Vertical 1
        , Level.Horizontal 1
        , Level.Vertical 1
        , Level.Horizontal -1
        , Level.Vertical 1
        , Level.Horizontal 2
        , Level.Vertical -3
        , Level.Horizontal 1
        , Level.Vertical 4
        , Level.Horizontal -2
        , Level.Vertical 1
        , Level.Horizontal -3
        , Level.Vertical -1
        , Level.Horizontal 1
        , Level.Vertical -1
        , Level.Horizontal -1
        , Level.Vertical -1
        , Level.Horizontal 1
        , Level.Vertical -1
        , Level.Horizontal -1
        , Level.Vertical -1
        ]
      }
    ]
  , floorItems =
    [ ( (1,1), Item.Flour )
    , ( (3,2), Item.Flour )
    , ( (1,3), Item.Yeast )
    , ( (2,5), Item.Yeast )
    , ( (1,5), Item.Yeast )
    , ( (3,5), Item.Flour )
    ]
  , obstacles =
    [ ( (5,2), Obstacle.Water )
    ]
  , recipes =
    [ { inputs = [Item.Flour, Item.Yeast]
      , outputs = [Item.BreadLoaf]
      }
    , { inputs = [Item.BreadLoaf]
      , outputs = [Item.BreadSlice, Item.BreadSlice]
      }
    , { inputs = [Item.BreadSlice]
      , outputs = [Item.ToastSlice]
      }
    , { inputs = [Item.ToastSlice, Item.ToastSlice]
      , outputs = [Item.Sandwich]
      }
    , { inputs = [Item.InnerTube]
      , outputs = [Item.RubberScrap]
      }
    ]
  , playerItems = [Item.InnerTube, Item.InnerTube, Item.Yeast]
  , playerLoc = (2, 2)
  , winLoc = (5, 1)
  , gridSize = (7,7)
  , facingLeft = True
  , banner = "Let's try this one again, but a little harder."
  }

