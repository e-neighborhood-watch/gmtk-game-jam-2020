module Levels.Sequence exposing
  ( levels
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Levels.Cycle as Cycle
import Levels.RiverCrossing as RiverCrossing
import Levels.Test as Test
import Levels.Switch as Switch
import Levels.SwitchHard as SwitchHard
import Levels.Consumption as Consumption
import Levels.Consumption2 as Consumption2
import Levels.End as End
import Level exposing (..)

levels : Nonempty Level
levels =
  Nonempty
    RiverCrossing.level
    [ Switch.level
    , SwitchHard.level
    , Cycle.level
    , Consumption.level
    , Consumption2.level
    , End.level
    ]

