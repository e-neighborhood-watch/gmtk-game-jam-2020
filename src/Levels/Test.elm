module Levels.Test exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 0)
      , paces =
        Nonempty
        ( Horizontal 5 )
        [ Vertical 3
        , Horizontal -1
        , Vertical 2
        , Horizontal -4
        , Vertical -5
        ]
      }
    ]
  , floorItems = []
  , obstacles = []
  , recipes = []
  , playerItems = []
  , playerLoc = (1, 1)
  , winLoc = (5, 5)
  , gridSize = (7,7)
  , facingLeft = True
  , banner = "Hello, World!"
  }
