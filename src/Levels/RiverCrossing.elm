module Levels.RiverCrossing exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Recipe exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 0)
      , paces =
        Nonempty
        ( Horizontal 5 )
        [ Vertical 2
        , Horizontal -5
        , Vertical -2
        ]
      }
    ]
  , floorItems =
    [ ( (2,2), Valve )
    , ( (3,1), RubberScrap )
    ]
  , obstacles =
    [ ( (4,2), Water )
    , ( (4,1), Water )
    ]
  , recipes =
    [ Recipe
      [ Valve
      , RubberScrap
      ]
      [ InnerTube
      ]
    ]
  , playerItems = []
  , playerLoc = (1, 1)
  , winLoc = (5, 2)
  , gridSize = (7,4)
  , facingLeft = True
  , banner = "Welcome!  You have been given a Compact Repurposer And Processor (C.R.A.P).  It's a fancy crafting machine which combines items into more useful items.  It uses a sophisticated algorithm to determine the most useful item to craft, so that you don't have to think!  At the bottom you will see the suggested recipes; just gather the items for these and it will automatically make the results.  Here you need a innertube to cross the river.\n\nYou can move around with the 'w' 'a' 's' 'd' keys or pass a turn with 'x'."
  }
