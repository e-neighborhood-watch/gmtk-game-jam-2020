module Levels.SwitchHard exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 0)
      , paces =
        Nonempty
        ( Level.Horizontal 1 )
        [ Level.Vertical 2
        , Level.Horizontal 3
        , Level.Vertical -2
        , Level.Horizontal 1
        , Level.Vertical 4
        , Level.Horizontal -5
        , Level.Vertical -4
        ]
      }
    ]
  , floorItems =
    [ ( (1,1), Item.Wood )
    , ( (1,2), Item.Nail )
    , ( (1,3), Item.Wire )
    ]
  , obstacles =
    [ ( (5,2), Obstacle.Gate )
    ]
  , recipes =
    [ { inputs = [Item.Nail, Item.Wood, Item.Wire]
      , outputs = [Item.Switch]
      }
    , { inputs = [Item.Nail, Item.Wire]
      , outputs = [Item.BarbedWire]
      }
    , { inputs = [Item.Nail, Item.Wood]
      , outputs = [Item.Bench]
      }
    ]
  , playerItems = []
  , playerLoc = (2, 4)
  , winLoc = (5, 1)
  , gridSize = (7,6)
  , facingLeft = True
  , banner = "Our patented C.R.A.P. algorithms make its ability find the most useful item almost uncanny!"
  }

