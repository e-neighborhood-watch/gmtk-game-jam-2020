module Levels.End exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)
import Recipe exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (1, 0)
      , paces =
        Nonempty
        ( Horizontal 3 )
        [ Vertical 1
        , Horizontal 1
        , Vertical 3
        , Horizontal -1
        , Vertical 1
        , Horizontal -3
        , Vertical -1
        , Horizontal -1
        , Vertical -3
        , Horizontal 1
        , Vertical -1
        ]
      }
    ]
  , floorItems = []
  , obstacles =
    [ ((1, 2), FireObstacle)
    , ((1, 3), FireObstacle)
    , ((1, 4), FireObstacle)
    , ((2, 1), FireObstacle)
    , ((2, 2), FireObstacle)
    , ((2, 3), FireObstacle)
    , ((2, 4), FireObstacle)
    , ((2, 5), FireObstacle)
    , ((3, 1), FireObstacle)
    , ((3, 2), FireObstacle)
    , ((3, 4), FireObstacle)
    , ((3, 5), FireObstacle)
    , ((4, 1), FireObstacle)
    , ((4, 2), FireObstacle)
    , ((4, 3), FireObstacle)
    , ((4, 4), FireObstacle)
    , ((4, 5), FireObstacle)
    , ((5, 2), FireObstacle)
    , ((5, 3), FireObstacle)
    , ((5, 4), FireObstacle)
    ]
  , recipes =
    [ Recipe
      [ Valve
      , RubberScrap
      ]
      [ InnerTube
      ]
    ]
  , playerItems =
    [ Valve
    , RubberScrap
    ]
  , playerLoc = (3, 3)
  , winLoc = (10, 10)
  , gridSize = (7,7)
  , facingLeft = False
  , banner = "Congratulations!\nYour demo of the C.R.A.P. is now complete. As you have witnessed, this crafting machine will unlock heretofore unimaginable possibilities for all of humanity. There are no more levels, so you may now use the C.R.A.P. however you please.\nEnjoy!"
  }
