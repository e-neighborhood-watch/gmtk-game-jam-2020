module Levels.Switch exposing
  ( level
  )

import List.Nonempty
  exposing
    ( Nonempty (..)
    )

import Level exposing (..)
import Item exposing (..)
import Obstacle exposing (..)

level : Level
level =
  { walls =
    [ { origin =
        (0, 0)
      , paces =
        Nonempty
        ( Level.Horizontal 1 )
        [ Level.Vertical 2
        , Level.Horizontal 1
        , Level.Vertical -2
        , Level.Horizontal 1
        , Level.Vertical 2
        , Level.Horizontal 1
        , Level.Vertical -2
        , Level.Horizontal 1
        , Level.Vertical 4
        , Level.Horizontal -5
        , Level.Vertical -4
        ]
      }
    ]
  , floorItems =
    [ ( (1,1), Item.Wood )
    , ( (2,3), Item.Nail )
    , ( (3,1), Item.Wire )
    ]
  , obstacles =
    [ ( (5,2), Obstacle.Gate )
    ]
  , recipes =
    [ { inputs = [Item.Nail, Item.Wood, Item.Wire]
      , outputs = [Item.Switch]
      }
    , { inputs = [Item.Nail, Item.Wire]
      , outputs = [Item.BarbedWire]
      }
    , { inputs = [Item.Nail, Item.Wood]
      , outputs = [Item.Bench]
      }
    ]
  , playerItems = []
  , playerLoc = (2, 4)
  , winLoc = (5, 1)
  , gridSize = (7,6)
  , facingLeft = True
  , banner = "A switch can open gates when placed next to them.  You can press 'z' to drop the first item in your inventory at your feet and 'q' or 'e' to cycle your inventory.  If you need to restart the level hit 'r'."
  }

