module Records exposing (..)

import Math.Vector2 exposing (Vec2)
import Math.Vector4 exposing (Vec4)
import WebGL.Texture exposing (Texture)


type alias Vertexed a =
    { a
        | abs_position : Vec2
        , rel_position : Vec2
        , canvasSize : Vec2
    }


type alias Sized a =
    { a
        | size : Vec2
    }


type alias Textured a =
    { a | texture : Texture }


type alias Null a =
    { a | n : () }


null : Null {}
null =
    { n = () }
