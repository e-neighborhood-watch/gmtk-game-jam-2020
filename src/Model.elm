module Model exposing
  ( PrgmModel
  , TextureStatus (..)
  , TextureRecord
  , nextLevel
  )

import Level exposing (..)

import WebGL.Texture as Texture exposing (Texture)
import List.Nonempty
  exposing
    ( Nonempty(..)
    )

type TextureStatus
    = TextureLoaded TextureRecord
    | TextureErrored String
    | TextureLoading

type alias TextureRecord =
  { protagL : Texture
  , protagR : Texture
  , protagFL : Texture
  , protagFR : Texture
  , exit : Texture
  , item : Texture
  , nail : Texture
  , wood : Texture
  , fire : Texture
  , bench : Texture
  , switch : Texture
  , wire : Texture
  , barbedWire : Texture
  , flour : Texture
  , yeast : Texture
  , rubberScrap : Texture
  , valve : Texture
  , innertube : Texture
  , water : Texture
  , gate : Texture
  , gateOpen : Texture
  , fireObstacle : Texture
  , schematic : Texture
  , airplane : Texture
  , glassShards : Texture
  , moltenGlass : Texture
  , bottle : Texture
  , hammer : Texture
  , message : Texture
  , breadLoaf : Texture
  , breadSlice : Texture
  , toastSlice : Texture
  , sandwich : Texture
  }

type alias PrgmModel =
  { textureStatus : TextureStatus
  , level : Level
  , upcoming : Nonempty Level
  }

nextLevel : PrgmModel -> PrgmModel
nextLevel model =
  case
    model.upcoming
  of
    -- If there is another level make it the current level
    Nonempty curLvl (nxtLvl :: lvls) ->
      { model
      | level = nxtLvl
      , upcoming = Nonempty nxtLvl lvls -- the next level stays on upcoming so we can reset to it.
      }
    -- If we are at the last level just reset it
    -- A more permanent solution is needed in the future!
    Nonempty curLvl [] ->
      { model
      | level = curLvl
      }
