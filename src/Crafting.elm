module Crafting exposing
  ( autoCraft
  )

import Item exposing
  ( Item
  )
import Recipe exposing
  ( Recipe
  )

-- Attempt to perform one craft if possible.
-- Prioritizes recipes earlier in the list.
-- Only crafts one item
autoCraft : List Recipe -> List Item -> (Maybe Int, List Item)
autoCraft =
  let
    autoCraft1 count recipes items =
      case
        recipes
      of
        [] ->
          ( Nothing
          , items
          )
        r :: rs ->
          case
            craft r (List.reverse items)
          of
            Nothing ->
              autoCraft1 (count + 1) rs items
            Just newItems ->
              ( Just count
              , List.reverse newItems
              )
  in
    autoCraft1 0

removeFirst : a -> List a -> List a
removeFirst remover things =
  case
    things
  of
    [] ->
      []
    t :: ts ->
      if
        t == remover
      then
        ts
      else
        t :: removeFirst remover ts
  
craft : Recipe -> List Item -> Maybe (List Item)
craft recipe items =
  case
    recipe.inputs
  of
    -- No inputs left unconsumed
    [] ->
      Just (recipe.outputs ++ items)
    inputs ->
      case
        items
      of
        -- No items left to use
        [] ->
          Nothing
        i :: is ->
          if
            List.member i inputs
          then
            craft (Recipe (removeFirst i inputs) recipe.outputs) is
          else
            Maybe.map ((::) i) (craft recipe is)
