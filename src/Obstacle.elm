module Obstacle exposing
  ( Obstacle (..)
  )

type Obstacle
  = Gate
  | Water
  | FireObstacle
