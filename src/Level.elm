module Level exposing
  ( Pace (..)
  , Level 
  , Wall
  , validMove
  , pickup
  , itemAt
  , obstacleAt
  , validMoveObstacles
  )

import Recipe exposing
  ( Recipe
  )
import Item exposing
  ( Item
  )
import Obstacle exposing
  ( Obstacle
  )
import List.Nonempty as NonEmpty
  exposing
    ( Nonempty(..)
    )

type alias Wall =
    { origin : ( Int, Int )
    , paces : Nonempty Pace
    }

type Pace
    = Horizontal Int
    | Vertical Int

type alias Level =
  { walls : List Wall
  , floorItems : List ((Int, Int), Item)
  , obstacles: List ((Int, Int), Obstacle)
  , recipes : List Recipe
  , playerItems : List Item 
  , playerLoc : (Int, Int)
  , facingLeft : Bool
  , winLoc : (Int, Int)
  , gridSize : ( Int, Int )
  , banner : String
  }

-- Retrieves the item at a location
itemAt : (Int, Int) -> Level -> Maybe Item
itemAt loc lvl =
  let
    lookup key dict =
      case
        dict
      of
        [] ->
          Nothing
        (k, v) :: ds ->
          if
            k == key
          then
            Just v
          else
            lookup key ds
  in
    lookup loc lvl.floorItems

-- Removes the item at a location from the floor
--
-- Assumes there is only one item at a location
removeItemAt : (Int, Int) -> Level -> Level
removeItemAt loc lvl =
  let
    remove key dict =
      case
        dict
      of
        [] ->
          []
        (k, v) :: ds ->
          if
            k == key
          then
            ds -- Assume there is only one
          else
            (k, v) :: remove key ds
  in
    { lvl
    | floorItems =
      remove loc lvl.floorItems
    }

-- Picks up an item at the player's location
--
-- Assumes there is only one item at a location
pickup : Level -> Level
pickup lvl =
  case
    itemAt lvl.playerLoc lvl
  of
    Nothing ->
      lvl
    Just i ->
      { lvl
      | playerItems = i :: lvl.playerItems
      }
        |> removeItemAt lvl.playerLoc

-- Retrieves the obstacle at a location
obstacleAt : (Int, Int) -> Level -> Maybe Obstacle
obstacleAt loc lvl =
  let
    lookup key dict =
      case
        dict
      of
        [] ->
          Nothing
        (k, v) :: ds ->
          if
            k == key
          then
            Just v
          else
            lookup key ds
  in
    lookup loc lvl.obstacles
-- Determine if a move collides with a wall
--
-- Assumes that the start and end differ by 1 in the manhatan distance
validMove : ( Int, Int ) -> Level -> Bool
validMove end level = 
  List.all (\wall -> validMoveWalls level.playerLoc end (NonEmpty.toList wall.paces) wall.origin) level.walls && validMoveObstacles end level

validMoveWalls : (Int, Int) -> (Int, Int) -> List Pace -> (Int, Int) -> Bool
validMoveWalls (x1, y1) (x2, y2) walls (ox, oy) = 
  case walls of
    [] -> True
    ( wall :: moreWalls ) ->
      case wall of
        Horizontal x ->
          if
            y1 == y2
          then
            validMoveWalls (x1, y1) (x2, y2) moreWalls (ox+x, oy)
          else
            if
              (min y1 y2) <= oy && oy < (max y1 y2) && (min (ox+x) ox) + 1 <= x1 && x1 < (max (ox+x) ox) + 1
            then False
            else
              validMoveWalls (x1, y1) (x2, y2) moreWalls (ox+x, oy)
         
        Vertical y ->
          if
            x1 == x2
          then
            validMoveWalls (x1, y1) (x2, y2) moreWalls (ox, oy+y)
          else
            if
              (min x1 x2) <= ox && ox < (max x1 x2) && (min oy (oy+y)) +1 <= y1 && y1 < (max oy (oy+y)) + 1
            then False
            else
              validMoveWalls (x1, y1) (x2, y2) moreWalls (ox, oy+y)

validMoveObstacles : (Int, Int) -> Level -> Bool
validMoveObstacles loc level =
  case
    obstacleAt loc level
  of
    Nothing -> True
    Just Obstacle.Gate ->
      let (x,y) = loc in
        List.member (Just Item.Switch)
          [ itemAt (x, y+1) level
          , itemAt (x, y-1) level
          , itemAt (x+1, y) level
          , itemAt (x-1, y) level
          ]
    Just Obstacle.Water ->
      List.member Item.InnerTube level.playerItems
    Just Obstacle.FireObstacle ->
      False

