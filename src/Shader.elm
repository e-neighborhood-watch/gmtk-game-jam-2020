module Shader exposing
    ( textureFragment
    , fragmentWall
    , vertex
    )

import Math.Vector2 as Vec2 exposing (Vec2)
import Math.Vector4 as Vec4 exposing (Vec4)
import Records exposing (..)
import WebGL exposing (Shader)
import WebGL.Texture as Texture exposing (Texture)


vertex : Shader (Vertexed v) (Null n) { pos : Vec2 }
vertex =
    [glsl|
      attribute vec2 abs_position;
      attribute vec2 rel_position;
      attribute vec2 canvasSize;
      varying vec2 pos;

      vec2 rescale(vec2 pos, vec2 maxSize) {
        return 2.0*pos/maxSize - 1.0;
      }

      void main () {
        gl_Position = vec4(rescale(abs_position, canvasSize), 0.0, 1.0);
        pos = rel_position;
      }

    |]


textureFragment : Shader {} (Textured t) { pos : Vec2 }
textureFragment =
    [glsl|
      precision mediump float;
      varying vec2 pos;
      uniform sampler2D texture;
      //const float gridSize = 1.0/7.0;
      //const float thickness = 0.005;

      void main () {
        vec4 color = texture2D(texture, pos);
        //vec4 colorDerivativeX = dFdx(texture2D(texture, pos));
        //vec4 colorDerivativeY = dFdy(texture2D(texture, pos));
        //float foo = mod(pos.x, gridSize);
        //float bar = mod(pos.y, gridSize);
        //if (foo < thickness || foo > gridSize - thickness || bar < thickness || bar > gridSize - thickness) {
        //  color = vec4(0, 0, 0, 1);
        //}
        gl_FragColor = color;
      }

    |]


fragmentWall : Shader {} { a | connectUp : Bool, connectDown : Bool, connectLeft : Bool, connectRight : Bool } { pos : Vec2 }
fragmentWall =
    [glsl|
      precision mediump float;
      varying vec2 pos;
      uniform bool connectUp;
      uniform bool connectDown;
      uniform bool connectLeft;
      uniform bool connectRight;
      const float halfSpacing = 0.06;
      const float thickness = 0.04;

      /*
      float rand(vec2 co){
        return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * 2.0 - 1.0;
      }

      float bump(float bumpMiddle, float bumpLength, float x) {
        float shiftedX = (x - bumpMiddle) / bumpLength;
        if (x < 1.0 || x > -1.0) {
          return 0;
        } else {
          return exp(-pow(1.0 - pow(x, 2.0)));
        }
      }
      */

      void main () {
        vec4 color = vec4(0, 0, 0, 0);
        vec2 signedHalfDist = pos - 0.5; // + 0.006*rand(pos);
        vec2 halfDist = abs(signedHalfDist);
        float lineEnd = halfSpacing + thickness;
        float belowCenter = 0.5 - halfSpacing;
        float aboveCenter = 0.5 + halfSpacing;
        if (connectUp) {
          if (pos.y > aboveCenter && halfDist.x > halfSpacing && halfDist.x < lineEnd) {
            color.a = 1.0;
          }
        } else {
          if (halfDist.x < lineEnd && signedHalfDist.y > halfSpacing && signedHalfDist.y < lineEnd) {
            color.a = 1.0;
          }
        }
        if (connectDown) {
          if (pos.y < belowCenter && halfDist.x > halfSpacing && halfDist.x < lineEnd) {
            color.a = 1.0;
          }
        } else {
          if (halfDist.x < lineEnd && -signedHalfDist.y > halfSpacing && -signedHalfDist.y < lineEnd) {
            color.a = 1.0;
          }
        }
        if (connectRight) {
          if (pos.x > aboveCenter && halfDist.y > halfSpacing && halfDist.y < lineEnd) {
            color.a = 1.0;
          }
        } else {
          if (halfDist.y < lineEnd && signedHalfDist.x > halfSpacing && signedHalfDist.x < lineEnd) {
            color.a = 1.0;
          }
        }
        if (connectLeft) {
          if (pos.x < belowCenter && halfDist.y > halfSpacing && halfDist.y < lineEnd) {
            color.a = 1.0;
          }
        } else {
          if (halfDist.y < lineEnd && -signedHalfDist.x > halfSpacing && -signedHalfDist.x < lineEnd) {
            color.a = 1.0;
          }
        }
        gl_FragColor = color;
      }

    |]
