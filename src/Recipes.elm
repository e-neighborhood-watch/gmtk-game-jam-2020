module Recipes exposing
  ( recipesHtml
  )

import Level exposing (..)
import Recipe exposing (..)
import Crafting exposing (..)
import Item exposing
  ( Item
  , inventoryText
  )

import Html exposing
  ( Html
  , text
  , br
  )
import Html.Attributes as Html

recipeName : Recipe -> String
recipeName recipe =
  inventoryText recipe.inputs ++ " => " ++ inventoryText recipe.outputs

recipesHtml : Level -> List (Html a)
recipesHtml level =
  let
    (nextR, _) = autoCraft level.recipes level.playerItems
    recipesHtml1 count recipes =
      let
        thisLine =
          if
            Just count == nextR
          then
            recipeName >> (++) "> " >> text
          else
            recipeName >> (++) ". " >> text
       in
         case
           recipes
         of
           [] ->
             []
           [ r ] ->
             [ thisLine r ]
           r1 :: (r2 :: rs) ->
             thisLine r1 :: br [] [] :: recipesHtml1 (count + 1) (r2 :: rs)
  in
    recipesHtml1 0 level.recipes
