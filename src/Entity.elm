module Entity
  exposing
    ( fromWalls
    , textureRectangle
    , textureItem
    , textureObstacle
    )

import Level exposing (Level)
import Item exposing (..)
import Obstacle exposing (..)
import Model exposing (TextureRecord)
import Math.Vector2 as Vec2 exposing (Vec2, vec2)
import List.Nonempty as NonEmpty
import Shader
import Records exposing (..)
import Util exposing (..)
import WebGL
import WebGL.Settings.Blend as Blend
import WebGL.Texture as Texture exposing (Texture)

textureItem : Vec2 -> TextureRecord -> ((Int, Int), Item) -> WebGL.Entity
textureItem gridSize texture (loc, item) =
  textureRectangle
    (intPairToVec2 loc)
    (vec2 1 1)
    gridSize
    ( case
        item
      of
        Wood ->
          texture.wood
        Fire ->
          texture.fire
        Bench ->
          texture.bench
        Switch ->
          texture.switch
        Wire ->
          texture.wire
        BarbedWire ->
          texture.barbedWire
        Flour ->
          texture.flour
        Yeast ->
          texture.yeast
        RubberScrap ->
          texture.rubberScrap
        Valve ->
          texture.valve
        InnerTube ->
          texture.innertube
        Nail ->
          texture.nail
        Schematic   -> texture.schematic
        Airplane    -> texture.airplane
        GlassShards -> texture.glassShards
        MoltenGlass -> texture.moltenGlass
        Bottle      -> texture.bottle
        Hammer      -> texture.hammer
        MessageInABottle -> texture.message
        BreadLoaf -> texture.breadLoaf
        BreadSlice -> texture.breadSlice
        ToastSlice -> texture.toastSlice
        Sandwich -> texture.sandwich
        -- _ -> texture.item
    )

textureObstacle : Vec2 -> TextureRecord -> Level -> ((Int, Int), Obstacle) -> WebGL.Entity
textureObstacle gridSize texture level (loc, obstacle) =
  textureRectangle
    (intPairToVec2 loc)
    (vec2 1 1)
    gridSize
    ( case
        obstacle
      of
        Gate ->
          if
            Level.validMoveObstacles loc level
          then
            texture.gateOpen
          else
            texture.gate
        Water ->
          texture.water
        FireObstacle ->
          texture.fireObstacle
    )

textureRectangle : Vec2 -> Vec2 -> Vec2 -> Texture -> WebGL.Entity
textureRectangle bottomLeft size canvasSize texture =
  WebGL.entityWith
    [ Blend.add Blend.srcAlpha Blend.oneMinusSrcAlpha ]
    Shader.vertex
    Shader.textureFragment
    (rectangle bottomLeft size canvasSize)
    { texture = texture , n = () }

rectangle : Vec2 -> Vec2 -> Vec2 -> WebGL.Mesh (Vertexed {})
rectangle bottomLeft size canvasSize =
    WebGL.triangles
        [ ( { rel_position = vec2 0 0
            , abs_position = bottomLeft
            , canvasSize = canvasSize
            }
          , { rel_position = vec2 0 1
            , abs_position = Vec2.getY size |> vec2 0 |> Vec2.add bottomLeft
            , canvasSize = canvasSize
            }
          , { rel_position = vec2 1 0
            , abs_position = vec2 (Vec2.getX size) 0 |> Vec2.add bottomLeft
            , canvasSize = canvasSize
            }
          )
        , ( { rel_position = vec2 1 1
            , abs_position = Vec2.add bottomLeft size
            , canvasSize = canvasSize
            }
          , { rel_position = vec2 1 0
            , abs_position = vec2 (Vec2.getX size) 0 |> Vec2.add bottomLeft
            , canvasSize = canvasSize
            }
          , { rel_position = vec2 0 1
            , abs_position = Vec2.getY size |> vec2 0 |> Vec2.add bottomLeft
            , canvasSize = canvasSize
            }
          )
        ]

fromWalls : (Int, Int) -> Level.Wall -> List WebGL.Entity
fromWalls gridSize wall =
  let
    entitiesFromWallSection : ( Int, Int ) -> Level.Pace -> List Level.Pace -> List WebGL.Entity
    entitiesFromWallSection ( x, y ) prevPace remainingPaces =
        case remainingPaces of
            (Level.Horizontal length) :: rest ->
                WebGL.entity
                    Shader.vertex
                    Shader.fragmentWall
                    (rectangle
                        (Vec2.add (vec2 0.5 0.5) (intPairToVec2 ( x, y )))
                        (vec2 1 1)
                        (intPairToVec2 gridSize)
                    )
                    (case prevPace of
                        Level.Horizontal prevLength ->
                            { connectDown = False
                            , connectUp = False
                            , connectLeft = length < 0 || prevLength > 0
                            , connectRight = length > 0 || prevLength < 0
                            , n = ()
                            }

                        Level.Vertical prevLength ->
                            { connectDown = prevLength > 0
                            , connectUp = prevLength < 0
                            , connectLeft = length < 0
                            , connectRight = length > 0
                            , n = ()
                            }
                    )
                    :: (if abs length <= 1 then
                            []

                        else
                            [ WebGL.entity
                                Shader.vertex
                                Shader.fragmentWall
                                (rectangle
                                    (Vec2.add (vec2 0.5 0.5) (intPairToVec2 ( x + 1 - clamp 0 (abs length) (-length), y )))
                                    (vec2 (toFloat (abs length - 1)) 1)
                                    (intPairToVec2 gridSize)
                                )
                                { connectDown = False
                                , connectUp = False
                                , connectLeft = True
                                , connectRight = True
                                , n = ()
                                }
                            ]
                       )
                    ++ entitiesFromWallSection ( x + length, y ) (Level.Horizontal length) rest

            (Level.Vertical length) :: rest ->
                WebGL.entity
                    Shader.vertex
                    Shader.fragmentWall
                    (rectangle
                        (Vec2.add (vec2 0.5 0.5) (intPairToVec2 ( x, y )))
                        (vec2 1 1)
                        (intPairToVec2 gridSize)
                    )
                    (case prevPace of
                        Level.Horizontal prevLength ->
                            { connectDown = length < 0
                            , connectUp = length > 0
                            , connectLeft = prevLength > 0
                            , connectRight = prevLength < 0
                            , n = ()
                            }

                        Level.Vertical prevLength ->
                            { connectDown = length < 0 || prevLength > 0
                            , connectUp = length > 0 || prevLength < 0
                            , connectLeft = False
                            , connectRight = False
                            , n = ()
                            }
                    )
                    :: (if abs length <= 1 then
                            []

                        else
                            [ WebGL.entity
                                Shader.vertex
                                Shader.fragmentWall
                                (rectangle
                                    (Vec2.add (vec2 0.5 0.5) (intPairToVec2 ( x, y + 1 - clamp 0 (abs length) (-length) )))
                                    (vec2 1 (toFloat (abs length - 1)))
                                    (intPairToVec2 gridSize)
                                )
                                { connectDown = True
                                , connectUp = True
                                , connectLeft = False
                                , connectRight = False
                                , n = ()
                                }
                            ]
                       )
                    ++ entitiesFromWallSection ( x, y + length ) (Level.Vertical length) rest

            [] ->
                []

  in
    entitiesFromWallSection wall.origin (NonEmpty.last wall.paces) (NonEmpty.toList wall.paces)
